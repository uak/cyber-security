Hello,
A phishing website is using your infrastructure to manage their fraudulent business.
They are stealing people fund by showing a fake web wallet.

- Website: FAKE_WEBSITE_URL
- IP address: FAKE_WEBSITE_IP
- archive copy: LINK_TO_ARCHIVED_COPY


Original website:
- https://www.bitcoincashnode.org/

Recognizable website linking to the original website:
- https://www.bitcoin.com/full-nodes/
- https://launchpad.net/~bitcoin-cash-node/+archive/ubuntu/ppa
- https://aur.archlinux.org/packages/bitcoin-cash/
- https://blockchair.com/bitcoin/whitepaper
- https://cash.coin.dance/development


Official project source code repository:
- https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node

Original project blogging about fake wallets:
- https://read.cash/@bitcoincashnode/psa-beware-of-accounts-impersonating-bchn-d07f306d

Regards,
YOUR NAME
